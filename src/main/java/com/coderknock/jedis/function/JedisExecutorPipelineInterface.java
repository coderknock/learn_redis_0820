package com.coderknock.jedis.function;

import redis.clients.jedis.Pipeline;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-06-22
 * @QQGroup 213732117 【购买了讲座的童鞋请加 660767582 验证信息输入 sf 用户名】
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
@FunctionalInterface
public interface JedisExecutorPipelineInterface {
    void execute(Pipeline pipeline);
}
