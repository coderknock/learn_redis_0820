import com.coderknock.algorithm.GeoHash;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-08-07
 * @QQGroup 213732117 【购买了讲座的童鞋请加 660767582 验证信息输入 sf 用户名】
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
@DisplayName("GeoHash算法测试")
public class GeoHashTest {

    @DisplayName("验证")
    @ParameterizedTest
    @CsvFileSource(resources = "/geoTest.csv")
    public void GeoHashTest(double lat, double lon) {
        GeoHash geohash = GeoHash.getInstance();
        String s = geohash.encode(lat, lon);
        System.out.println(s);
        double[] geo = geohash.decode(s);
        assertAll(() -> assertEquals(lat, geo[0]),
                () -> assertEquals(lon, geo[1]));
    }

}
