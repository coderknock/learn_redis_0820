--声明一个数组
local result={};
local listKey=KEYS[1];
local num=ARGV[1];

--获取到数据
local listNum=redis.call("LRANGE",listKey,0,-1);
redis.log(redis.LOG_WARNING,"GET listNum");
for i=1,#listNum
do
    local val=listNum[i];
    if(tonumber(val)>=tonumber(num)) then
        table.insert(result,1,val);
    end
end
redis.log(redis.LOG_WARNING,"return");
return result;

