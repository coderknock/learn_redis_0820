package redisson;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RGeo;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-08-07
 * @QQGroup 213732117 【购买了讲座的童鞋请加 660767582 验证信息输入 sf 用户名】
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
@DisplayName("Redisson 使用演示")
public class Demo {
    @Test
    @DisplayName("简单测试")
    public void test() {
        Config config = null;
        RedissonClient redisson = null;
        try {
            URI uri = Demo.class.getClassLoader().getResource("redis.json").toURI();
            config = Config.fromJSON(new File(uri));
            redisson = Redisson.create(config);
            RGeo rGeo = redisson.getGeo("tour");
            assertNotNull(rGeo);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            if (redisson != null) {
                redisson.shutdown();
            }
        }
    }
}
