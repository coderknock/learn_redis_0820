import com.coderknock.jedis.executor.JedisExecutor;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-08-19
 * @QQGroup 213732117 【购买了讲座的童鞋请加 660767582 验证信息输入 sf 用户名】
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
public class JDBCTest {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "程序猿");
        map.put(2, "产品汪");
        map.put(3, "设计狮");
        map.put(4, "温和");
        map.put(5, "暴躁");
        map.put(6, "细心");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&generateSimpleParameterMetadata=true&useSSL=true", "root", "admin123.");
             PreparedStatement pst = connection.prepareStatement("select user_id,tag_id from tag_user");
             ResultSet resultSet = pst.executeQuery()) {

            JedisExecutor.executeWithPiplineNR(jedis -> {
                try {
                    while (resultSet.next()) {
                        jedis.setbit("tag:" + resultSet.getInt(2), resultSet.getInt(1), true);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
